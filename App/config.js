export const colors = {
	primary: '#d642a0',
	primaryLight: '#ffbfe7',
	secondry: '#1f419b',
	gray: '#aaa',
	grayLight: '#eee'
};

export const font = {
	size: {
		main: 16
	}
};

export const imageEndPoint =
	'https://s3.ap-south-1.amazonaws.com/s3.api.star.6.coderwitz.com';

const apiEndpoint = 'https://api.star.6.coderwitz.com/api/v1';
export const API = {
	login: `${apiEndpoint}/user/login/facebook`,
	photoCreate: `${apiEndpoint}/photo/create`,
	userPhotoUpdate: `${apiEndpoint}/user/photo/update`,
	userDetails: `${apiEndpoint}/user`,
	education: `${apiEndpoint}/education`,
	occupation: `${apiEndpoint}/occupation`,
	interest: `${apiEndpoint}/interest`,
	question: `${apiEndpoint}/question`,
	religion: `${apiEndpoint}/religion`,
	profileUpdate: `${apiEndpoint}/user/profile/update`,
	searchSettingUpdate: `${apiEndpoint}/user/search/setting/update`,
	matches: `${apiEndpoint}/user/match`,
	matchesChatable: `${apiEndpoint}/user/match/chatable`,
	matchAction: `${apiEndpoint}/user/match/action`,
	matchCreate: `${apiEndpoint}/user/match/create`,
	messageCreate: `${apiEndpoint}/user/message/create`,
	userSearch: `${apiEndpoint}/user/search`,
	messageChat: (user_id) => `${apiEndpoint}/user/message/chat/${user_id}/beginning`
};
