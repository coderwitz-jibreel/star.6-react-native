import React from 'react';
import {
	StyleSheet,
	TouchableOpacity,
	Text,
	AsyncStorage,
	Alert,
	BackHandler
} from 'react-native';
import {
	createDrawerNavigator,
	SafeAreaView,
	DrawerItems
} from 'react-navigation';

import Dashboard from '../views/Dashboard';
import Settings from '../views/Settings';
import Messages from '../views/Messages';
import Search from '../views/Search';
import Profile from '../views/Profile';

import { colors } from '../config';

export default (SideMenu = createDrawerNavigator(
	{
		Home: {
			screen: Dashboard
		},
		Messages: {
			screen: Messages
		},
		Search: {
			screen: Search
    },
    Profile: {
			screen: Profile
		},
		Settings: {
			screen: Settings
		}
	},
	{
		contentComponent: props => (
			<SafeAreaView
				style={{ flex: 1 }}
				forceInset={{ top: 'always', horizontal: 'never' }}
			>
				<DrawerItems activeTintColor={colors.primary} {...props} />
				<TouchableOpacity
					style={{ marginTop: 'auto', marginBottom: 0 }}
					onPress={() =>
						Alert.alert(
							'Log out',
							'Do you want to logout?',
							[
								{
									text: 'Cancel',
									onPress: () => {
										return null;
									}
								},
								{
									text: 'Confirm',
									onPress: () => {
										AsyncStorage.clear();
										BackHandler.exitApp();
									}
								}
							],
							{ cancelable: false }
						)
					}
				>
					<Text
						style={{
							margin: 16,
							fontWeight: 'bold',
							color: colors.textColor
						}}
					>
						Logout
					</Text>
				</TouchableOpacity>
			</SafeAreaView>
		),
		drawerOpenRoute: 'DrawerOpen',
		drawerCloseRoute: 'DrawerClose',
		drawerToggleRoute: 'DrawerToggle'
	}
));

// export const CustomDrawerContentComponent =

const styles = StyleSheet.create({
	container: {
		flex: 1
	}
});
