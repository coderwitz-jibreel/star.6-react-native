import React, { Component } from 'react';
import {
	View,
	Text,
	TextInput,
	TouchableOpacity,
	Image,
	StyleSheet,
	AsyncStorage
} from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { colors, font, API } from '../config';
import moment from 'moment';

const sendButton = require('../assets/send-button.png');

export default class SendMessage extends Component {
	constructor(props) {
		super(props);
		this.state = { message: '', isDateTimePickerVisible: false };
	}

	_showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

	_hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

	_handleDatePicked = date => {
		console.log('A date has been picked: ', date);
		const message = `I want to meet you at ${moment(date).format(
			'MMMM Do, YYYY'
		)}`;
		this.setState({ message });
		this._hideDateTimePicker();
	};

	async sendMessage() {
		const token = await AsyncStorage.getItem('token');
		const user_id = this.props.userId;
		const { message } = this.state;
		this.setState({ message: '' });
		let result = await fetch(API.messageCreate, {
			method: 'POST',
			body: JSON.stringify({ user_id, message }),
			headers: {
				'x-access-token': token,
				'Content-Type': 'application/json'
			}
		});
		result = await result.json();
		console.log(result);
		if (result.status) {
			this.props.onSend(result.response);
		}
	}

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.sendMessageInputContainer}>
					<TextInput
						placeholder='Type Mesage'
						name='message'
						onChangeText={message => this.setState({ message })}
						value={this.state.message}
						style={styles.sendMessageInput}
					/>
					<TouchableOpacity onPress={this.sendMessage.bind(this)}>
						<Image style={styles.sendButton} source={sendButton} />
					</TouchableOpacity>
				</View>
				<View styles={styles.footerContainer}>
					<Text style={styles.heading}>Plane a date</Text>
					<TouchableOpacity onPress={this._showDateTimePicker}>
						<Text style={styles.calendarButton}>Create Calendar Invite</Text>
					</TouchableOpacity>
					<DateTimePicker
						isVisible={this.state.isDateTimePickerVisible}
						onConfirm={this._handleDatePicked}
						onCancel={this._hideDateTimePicker}
					/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		paddingHorizontal: 15,
		paddingVertical: 5
	},
	sendMessageInputContainer: {
		backgroundColor: colors.grayLight,
		paddingHorizontal: 10,
		paddingVertical: 8,
		flexDirection: 'row',
		alignItems: 'center',
		borderWidth: 1,
		borderColor: 'rgba(0, 0, 0, 0.2)'
	},
	sendMessageInput: {
		flex: 1
	},
	sendButton: {
		height: 30,
		aspectRatio: 1
	},
	footerContainer: {
		alignItems: 'center'
	},
	heading: {
		fontSize: font.size.main,
		color: colors.secondry,
		textAlign: 'center',
		marginTop: 10
	},
	calendarButton: {
		fontSize: font.size.main,
		paddingHorizontal: 20,
		paddingVertical: 10,
		marginVertical: 5,
		borderColor: colors.secondry,
		color: colors.secondry,
		alignSelf: 'center',
		borderWidth: 1
	}
});
