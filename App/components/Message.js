import React from 'react';
import Avatar from './Avatar';
import { View, Text, Image, StyleSheet } from 'react-native';
import { colors } from '../config';

export default (Message = props => (
	<View style={styles.message}>
		<View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
			<Avatar image={props.image} show={!props.my} />
			<View
				style={[
					styles.messageTextContainer,
					props.my
						? styles.messageTextMyContainer
						: styles.messageTextOtherContainer
				]}
			>
				<View
					style={[
						styles.messageCone,
						props.my ? styles.messageConeMy : styles.messageConeOther
					]}
				/>
				<Text
					style={[
						styles.messageText,
						props.my ? styles.messageTextMy : styles.messageTextOther
					]}
				>
					{props.message}
				</Text>
			</View>
		</View>
		<View
			style={[
				styles.timeContainer,
				props.my ? styles.timeContainerMy : styles.timeContainerOther
			]}
		>
			<Text style={styles.time}>{props.time}</Text>
      <Image style={{aspectRatio: 1, width: 11, marginLeft: 5}} source={require('../assets/check.png')} />
		</View>
	</View>
));

const styles = StyleSheet.create({
	message: {
		width: '100%',
		padding: 10,
	},
	messageTextContainer: {
		flex: 0.7,
		borderRadius: 10,
		padding: 10
	},
	messageCone: {
		position: 'absolute',
		transform: [{ rotate: '45deg' }],
		width: 8,
		height: 8,
		top: 17
	},
	messageConeMy: {
		right: -5,
		borderColor: colors.gray,
		backgroundColor: '#fff',
		borderTopWidth: 1,
		borderRightWidth: 1
	},
	messageConeOther: {
		left: -4,
		backgroundColor: colors.gray
	},
	messageText: {},
	messageTextMy: {},
	messageTextOther: {
		color: '#fff'
	},
	messageTextMyContainer: {
		borderColor: colors.gray,
		borderWidth: 1,
		marginLeft: 'auto',
		marginRight: 5
	},
	messageTextOtherContainer: {
		backgroundColor: colors.gray
	},
	timeContainer: {
    flexDirection: 'row',
    paddingTop: 5,
	},
	timeContainerMy: {
		alignSelf: 'flex-end',
	},
	timeContainerOther: {
    marginLeft: 60
  },
	time: {
    fontSize: 11,
    color: colors.gray
	}
});
