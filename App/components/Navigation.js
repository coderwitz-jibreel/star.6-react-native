import { createStackNavigator } from 'react-navigation';
import { colors } from '../config';

import Home from '../views/Home';
import Dashboard from '../views/Dashboard';

const Navigation = createStackNavigator(
	{
		Home: { screen: Home },
		Dashboard: { screen: Dashboard }
	},
	{
		initialRouteName: 'Home',
		defaultNavigationOptions: {
			headerStyle: {
				backgroundColor: colors.primary
			},
			headerTintColor: '#fff',
			headerTitleStyle: {
				textAlign: 'center',
				color: '#ffffff'
			}
		}
	}
);

export default Navigation;
