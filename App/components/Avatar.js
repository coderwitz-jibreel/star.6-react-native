import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

export default (Avatar = props => {
	if (props.show) {
		return (
			<View style={styles.messageAvatar}>
				<Image
					style={styles.messageAvatarImage}
					source={props.image}
				/>
			</View>
		);
	}
	return <View />;
});

const styles = StyleSheet.create({
	messageAvatarImage: {
		borderRadius: 25,
		width: 50,
		aspectRatio: 1,
		marginRight: 10
	}
});
