import React from 'react';
import { View, Image } from 'react-native';

const logo = require('../assets/logo-w.png');
const LogoTitle = () => (
  <View style={{justifyContent: 'center', alignItems:'center', flex: 0.85}}>
    <Image source={logo} style={{ aspectRatio: 200/43, height: 26 }} />
  </View>
);

export default LogoTitle;
