import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import ModalSelector from 'react-native-modal-selector';
import {
	TouchableOpacity,
	View,
	Text,
	StyleSheet,
	Image,
	ScrollView,
	TextInput,
	AsyncStorage,
	Alert
} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { colors, API, font } from '../config';

searchIcon = require('../assets/search-icon.png');

class Search extends Component {
	static navigationOptions = ({ navigation }) => ({
		title: 'Search',
		headerLeft: (
			<TouchableOpacity
				onPress={() => navigation.openDrawer()}
				title='Info'
				color='#fff'
			>
				<Image
					style={{ height: 25, aspectRatio: 1, marginLeft: 10 }}
					source={require('../assets/menu.png')}
				/>
			</TouchableOpacity>
		),
		drawerLabel: 'Search'
	});

	constructor(props) {
		super(props);
		this.state = {
			scrollEnabled: false,
			values: [150, 190],
			ageValues: [30, 60],
			milesValues: [12, 2000],
			searchSettings: {
				heightStart: 5.0,
				heightEnd: 6.0,
				ageStart: 19.0,
				ageEnd: 25.0,
				distanceStart: 5.0,
				distanceEnd: 3000.0,
				religion_id: 1,
				education_id: 1,
				kosher: 'kosher 1'
			},
			educations: [],
			religions: []
		};
	}

	async componentWillMount() {
		try {
			const result = await this.getUserDetails();
			if (result.status) {
				this.setState({ searchSettings: result.response.search_setting });
			}
			this.getEducations();
			this.getReligion();
		} catch (error) {}
	}

	async getUserDetails() {
		const token = await AsyncStorage.getItem('token');
		try {
			const result = await fetch(API.userDetails, {
				method: 'GET',
				headers: {
					'x-access-token': token
				}
			});
			return result.json();
		} catch (error) {
			Alert.alert('ERROR', error.response);
			throw new Error(error);
		}
	}

	async getEducations() {
		const result = await fetch(API.education);
		const { status, response: educations } = await result.json();
		if (status) {
			this.setState({ educations });
		}
		return;
	}

	async getReligion() {
		const result = await fetch(API.religion);
		const { status, response: religions } = await result.json();
		if (status) {
			this.setState({ religions });
			console.log(religions);
		}
		return;
	}

	_enableScroll = () => this.setState({ scrollEnabled: true });
	_disableScroll = () => this.setState({ scrollEnabled: false });
	_onHeightValuesChangeFinish = heightValues => {
		const [heightStart, heightEnd] = heightValues;
		this.setState({
			searchSettings: { ...this.state.searchSettings, heightStart, heightEnd }
		});
		this.updateSettings();
		this._enableScroll();
	};

	_onAgeValuesChangeFinish = ageValues => {
		const [ageStart, ageEnd] = ageValues;
		this.setState({
			searchSettings: { ...this.state.searchSettings, ageStart, ageEnd }
		});
		this.updateSettings();
		this._enableScroll();
	};
	_onMilesValuesChangeFinish = milesValues => {
		const [distanceStart, distanceEnd] = milesValues;
		this.setState({
			searchSettings: {
				...this.state.searchSettings,
				distanceStart,
				distanceEnd
			}
		});
		this.updateSettings();
		this._enableScroll();
	};

	async updateSettings() {
		const token = await AsyncStorage.getItem('token');
		const settings = this.state.searchSettings;
		console.log(settings);
		const result = await fetch(API.searchSettingUpdate, {
			method: 'POST',
			body: JSON.stringify(settings),
			headers: {
				'x-access-token': token,
				'Content-Type': 'application/json'
			}
		});
		const json = await result.json();
		if (json.status) {
			console.log(json.response);
			return json;
		}
		console.log(json);
		Alert.alert('ERROR', json.response);
		return;
	}

	render() {
		let index = 0;
		const kosher = [
			{ key: index++, section: true, label: 'Kosher' },
			{ key: index++, label: `kosher one` },
			{ key: index++, label: `kosher two` },
			{ key: index++, label: `kosher three` }
		];
		return (
			<View style={styles.container}>
				<View style={[styles.searchContainer]}>
					<Image style={{ width: 30, aspectRatio: 1 }} source={searchIcon} />
					<TextInput
						style={{ flex: 1, paddingLeft: 10 }}
						placeholder='Search people'
					/>
				</View>
				<ScrollView scrollEnabled={this.state.scrollEnabled}>
					<View style={[styles.sliderContainer]}>
						<MultiSlider
							isMarkersSeparated={true}
							min={4.416667}
							max={6.75}
							values={[
								this.state.searchSettings.heightStart,
								this.state.searchSettings.heightEnd
							]}
							step={0.0833333}
							markerOffsetY={1}
							containerStyle={styles.containerStyle}
							trackStyle={styles.trackStyle}
							selectedStyle={styles.selectedStyle}
							customMarkerLeft={customHeightMarker}
							customMarkerRight={customHeightMarker}
							onValuesChangeStart={this._disableScroll}
							onValuesChangeFinish={this._onHeightValuesChangeFinish}
						/>
						<View
							style={{
								flexDirection: 'row',
								alignContent: 'space-between',
								width: '80%'
							}}
						>
							<View>
								<Text style={{ textAlign: 'center' }}>4'5"</Text>
								<Text style={{ textAlign: 'center' }}>feet</Text>
							</View>
							<View style={{ marginLeft: 'auto' }}>
								<Text style={{ textAlign: 'center' }}>6'10"</Text>
								<Text style={{ textAlign: 'center' }}>feet</Text>
							</View>
						</View>
					</View>

					<View style={[styles.sliderContainer]}>
						<MultiSlider
							isMarkersSeparated={true}
							min={18}
							max={80}
							values={[
								this.state.searchSettings.ageStart,
								this.state.searchSettings.ageEnd
							]}
							step={1}
							markerOffsetY={1}
							containerStyle={styles.containerStyle}
							trackStyle={styles.trackStyle}
							selectedStyle={styles.selectedStyle}
							customMarkerLeft={customMarker}
							customMarkerRight={customMarker}
							onValuesChangeStart={this._disableScroll}
							onValuesChangeFinish={this._onAgeValuesChangeFinish}
						/>
						<View
							style={{
								flexDirection: 'row',
								alignContent: 'space-between',
								width: '80%'
							}}
						>
							<View>
								<Text style={{ textAlign: 'center' }}>18</Text>
								<Text style={{ textAlign: 'center' }}>age</Text>
							</View>
							<View style={{ marginLeft: 'auto' }}>
								<Text style={{ textAlign: 'center' }}>80</Text>
								<Text style={{ textAlign: 'center' }}>age</Text>
							</View>
						</View>
					</View>

					<View style={[styles.sliderContainer]}>
						<MultiSlider
							isMarkersSeparated={true}
							min={0}
							max={6000}
							values={[
								this.state.searchSettings.distanceStart,
								this.state.searchSettings.distanceEnd
							]}
							step={1}
							markerOffsetY={1}
							containerStyle={styles.containerStyle}
							trackStyle={styles.trackStyle}
							selectedStyle={styles.selectedStyle}
							customMarkerLeft={customMarker}
							customMarkerRight={customMarker}
							onValuesChangeStart={this._disableScroll}
							onValuesChangeFinish={this._onMilesValuesChangeFinish}
						/>
						<View
							style={{
								flexDirection: 'row',
								alignContent: 'space-between',
								width: '80%'
							}}
						>
							<View>
								<Text style={{ textAlign: 'center' }}>0</Text>
								<Text style={{ textAlign: 'center' }}>miles</Text>
							</View>
							<View style={{ marginLeft: 'auto' }}>
								<Text style={{ textAlign: 'center' }}>6000</Text>
								<Text style={{ textAlign: 'center' }}>miles</Text>
							</View>
						</View>
					</View>

					<View style={{ paddingHorizontal: 10, marginTop: 20 }}>
						<View style={{ flexDirection: 'row' }}>
							<View style={[styles.picker, { flex: 1 }]}>
								<ModalSelector
									data={this.state.religions}
									initValue={
										this.state.searchSettings.religion
											? this.state.searchSettings.religion.name
											: 'Religion'
									}
									keyExtractor={item => item.id}
									labelExtractor={item => item.name}
									onChange={religion => {
										this.setState({
											searchSettings: {
												...this.state.searchSettings,
												religion_id: religion.id,
												religion
											}
                    });
                    this.updateSettings()
									}}
								/>
							</View>

							<View style={[styles.picker, { flex: 1 }]}>
								<ModalSelector
									data={kosher}
									initValue={this.state.searchSettings.kosher ? this.state.searchSettings.kosher :'Kosher'}
									onChange={option => {
                    this.setState({
											searchSettings: {
												...this.state.searchSettings,
												kosher: option.label,
											}
                    });
                    this.updateSettings()
									}}
								/>
							</View>
						</View>

						<View>
							<View style={styles.picker}>
								<ModalSelector
									data={this.state.educations}
									initValue={
										this.state.searchSettings.education
											? this.state.searchSettings.education.name
											: 'Highest level of education'
									}
									keyExtractor={item => item.id}
									labelExtractor={item => item.name}
									onChange={education => {
										const searchSettings = this.state.searchSettings;
										this.setState({
											searchSettings: {
												...searchSettings,
												education,
												education_id: education.id
											}
										});
										this.updateSettings()
									}}
								/>
							</View>
						</View>
					</View>
				</ScrollView>
			</View>
		);
	}
}

const customHeightMarker = props => (
	<View style={styles.markerThumb}>
		<View style={styles.markerThumbInner}>
			<Text style={styles.thumbText}>
				{`${Math.floor(props.currentValue)}'${Math.round(
					(props.currentValue - Math.floor(props.currentValue)) * 12
				)}"`}
			</Text>
		</View>
	</View>
);

const customMarker = props => (
	<View style={styles.markerThumb}>
		<View style={styles.markerThumbInner}>
			<Text style={styles.thumbText}>{props.currentValue}</Text>
		</View>
	</View>
);

const Navigation = createStackNavigator(
	{
		Search: { screen: Search }
	},
	{
		initialRouteName: 'Search',
		defaultNavigationOptions: {
			headerStyle: {
				backgroundColor: colors.primary
			},
			headerTintColor: '#fff',
			headerTitleStyle: {
				color: '#ffffff'
			}
		}
	}
);

export default Navigation;

const _shadow = {
	shadowColor: '#000',
	shadowOffset: {
		width: 0,
		height: 2
	},
	shadowOpacity: 0.23,
	shadowRadius: 2.62,
	elevation: 4
};

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	searchContainer: {
		..._shadow,
		zIndex: 99999,
		flexDirection: 'row',
		backgroundColor: '#fff',
		paddingVertical: 8,
		paddingHorizontal: 14
	},

	sliderContainer: {
		alignItems: 'center',
		alignContent: 'center',
		marginTop: 20
	},
	containerStyle: {
		alignSelf: 'center',
		paddingTop: 25
	},
	trackStyle: {
		height: 3
	},
	selectedStyle: {
		// ..._shadow,
		backgroundColor: colors.primary,
		// borderWidth: 1,
		height: 4
	},
	markerThumb: {
		..._shadow,
		width: 20,
		height: 20,
		borderWidth: 1,
		borderRadius: 100,
		borderColor: colors.primary,
		backgroundColor: '#fff'
	},
	markerThumbInner: {
		backgroundColor: colors.primary,
		borderRadius: 100,
		width: 15,
		height: 15,
		position: 'absolute',
		top: 1.49,
		left: 1.49
	},
	thumbText: {
		position: 'absolute',
		width: 50,
		textAlign: 'center',
		left: -15,
		top: -25
	},

	picker: {
		backgroundColor: colors.grayLight,
		borderColor: 'rgba(0,0,0,0.5)',
		borderWidth: 1,
		margin: 3,
		borderRadius: 3
	}
});
