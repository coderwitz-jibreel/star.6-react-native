import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import {
	TouchableOpacity,
	View,
	StyleSheet,
	Text,
	Image,
	Alert
} from 'react-native';
import { colors } from '../config';

const logo = require('../assets/logo.png');
const logoWidth = 240;

export default class Home extends Component {
	constructor(props) {
		super(props);
	}

	press() {
		AsyncStorage.setItem('isLoggedIn', true, error => {
			if (!error) {
				alert('update');
				this.setState({ isLoggedIn: true });
			}
		});
	}

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.top}>
					<Image style={styles.logo} source={logo} />
					<View style={{ flexDirection: 'row', paddingTop: 10 }}>
						<Text style={styles.logoText}>
							Best place for Jewish singles to find their match
						</Text>
					</View>
				</View>
				<View>
					<Image
						style={styles.homeImage}
						source={require('../assets/home-img.png')}
					/>
				</View>
				<View style={{ width: '100%', paddingHorizontal: 30 }}>
					<Text style={styles.footerText}>Create a new account or sign in</Text>
					<View
						style={{ flexDirection: 'row', justifyContent: 'space-between' }}
					>
						<TouchableOpacity
							style={{ width: '45%' }}
							onPress={this.props.onLogin}
						>
							<Text style={styles.button}>Get Started</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={{ width: '45%' }}
							onPress={this.props.onLogin}
						>
							<Text style={[styles.button, styles.buttonOutline]}>Sign In</Text>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'space-between',
		backgroundColor: '#ffffff',
		paddingTop: 70,
		paddingBottom: 30
	},
	logo: {
		width: logoWidth,
		height: (logoWidth * 84) / 317
	},
	top: {
		alignItems: 'center'
	},
	logoText: {
		flex: 0.6,
		textAlign: 'center'
	},
	homeImage: {
		width: 240,
		height: 240
	},
	footerText: {
		marginBottom: 20,
		textAlign: 'center'
	},
	button: {
		backgroundColor: colors.primary,
		paddingVertical: 10,
		borderRadius: 4,
		textAlign: 'center',
		color: '#ffffff',
		fontSize: 16
	},
	buttonOutline: {
		backgroundColor: 'transparent',
		borderWidth: 1,
		borderColor: colors.primary,
		color: colors.primary
	}
});
