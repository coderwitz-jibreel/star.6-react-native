import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import {
	TouchableOpacity,
	View,
	StyleSheet,
	Text,
	Image,
	FlatList,
	AsyncStorage
} from 'react-native';
import { colors, font, API, imageEndPoint } from '../config';
import LogoTitle from '../components/LogoTitle';
import Chat from './Chat';

import moment from 'moment';

class Messages extends Component {
	static navigationOptions = ({ navigation }) => ({
		headerTitle: <LogoTitle />,
		headerLeft: (
			<TouchableOpacity onPress={() => navigation.openDrawer()}>
				<Image
					style={{ height: 25, aspectRatio: 1, marginLeft: 10 }}
					source={require('../assets/menu.png')}
				/>
			</TouchableOpacity>
		),
		drawerLabel: 'Messages'
	});
	constructor(props) {
		super(props);
		this.state = {
			activeTab: 'messages',
			matches: [],
			chats: []
		};
	}

	componentWillMount() {
		this.getMatches();
		this.getChatsUsers();
	}

	async getMatches() {
		const token = await AsyncStorage.getItem('token');
		console.log(token);
		const result = await fetch(API.matches, {
			method: 'GET',
			headers: {
				'x-access-token': token
			}
		});
		const { response: matches } = await result.json();

		this.setState({ matches });
	}
	async getChatsUsers() {
		const token = await AsyncStorage.getItem('token');
		const result = await fetch(API.matchesChatable, {
			method: 'GET',
			headers: {
				'x-access-token': token
			}
		});
		let { status, response: chats } = await result.json();
		if (status) {
			this.setState({ chats: chats.chatBySent.concat(chats.chatByReceived) });
		}
	}

	render() {
		const list =
			this.state.activeTab == 'messages'
				? this.state.chats
				: this.state.matches;
		return (
			<View style={styles.container}>
				<View
					style={{
						flexDirection: 'row',
						justifyContent: 'flex-start',
						width: '100%',
						marginBottom: 10,
						paddingHorizontal: 10
					}}
				>
					<TouchableOpacity
						onPress={() => this.setState({ activeTab: 'messages' })}
					>
						<Text
							style={[
								styles.button,
								this.state.activeTab == 'messages' && styles.buttonActive
							]}
						>
							Messages
						</Text>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={() => this.setState({ activeTab: 'matches' })}
					>
						<Text
							style={[
								styles.button,
								this.state.activeTab == 'matches' && styles.buttonActive
							]}
						>
							Matches
						</Text>
					</TouchableOpacity>
				</View>
				<FlatList
					style={styles.messagesList}
					data={list}
					keyExtractor={item => item.id.toString()}
					renderItem={({ item }) => (
						<TouchableOpacity
							onPress={() =>
								this.props.navigation.navigate('Chat', {
									title: `${item.firstName} ${item.lastName}`,
									isMatch: this.state.activeTab == 'matches',
									user: item,
									onBack: () => {
										this.getChatsUsers();
										this.getMatches();
									}
								})
							}
						>
							<View style={styles.message}>
								{this.state.activeTab == 'matches' ? null : (
									<View style={[styles.onlineDot]} />
								)}
								<View style={styles.messageAvatar}>
									<Image
										style={styles.messageAvatarImage}
										source={{
											uri: item.photos.length
												? imageEndPoint + item.photos[0].path
												: 'http://placehold.it/150x150/cccccc'
										}}
									/>
								</View>
								<View style={{ flex: 1 }}>
									<Text
										numberOfLines={1}
										ellipsizeMode='tail'
										style={[styles.messageHeading]}
									>
										{item.firstName} {item.lastName}
									</Text>
									<Text
										numberOfLines={1}
										ellipsizeMode='tail'
										style={[styles.messageSubHeading]}
									>
										{this.state.activeTab == 'matches'
											? 'matched at: ' +
											  moment(item.created_at).format('MMMM DD, YYYY')
											: 'messages'}
									</Text>
								</View>
								<TouchableOpacity onPress={() => {}} title='Info' color='#fff'>
									<Image
										style={{ height: 25, aspectRatio: 1, marginLeft: 30 }}
										source={require('../assets/dot-menu.png')}
									/>
								</TouchableOpacity>
							</View>
						</TouchableOpacity>
					)}
				/>
			</View>
		);
	}
}

const Navigation = createStackNavigator(
	{
		Messages: { screen: Messages },
		Chat: { screen: Chat }
	},
	{
		initialRouteName: 'Messages',
		defaultNavigationOptions: {
			headerStyle: {
				backgroundColor: colors.primary
			},
			headerTintColor: '#fff',
			headerTitleStyle: {
				color: '#ffffff'
			}
		}
	}
);

export default Navigation;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: '#ffffff',
		width: '100%'
	},
	scrollContianer: {
		width: '100%'
	},
	button: {
		paddingHorizontal: 10,
		paddingVertical: 10,
		fontSize: font.size.main
	},
	buttonActive: {
		borderBottomColor: colors.primary,
		borderBottomWidth: 3
	},
	messagesList: {
		width: '100%'
	},
	message: {
		width: '100%',
		padding: 10,
		flexDirection: 'row',
		alignItems: 'center'
	},
	onlineDot: {
		position: 'absolute',
		backgroundColor: 'green',
		height: 5,
		width: 5,
		left: 10,
		top: 10,
		borderRadius: 20
	},
	messageAvatar: {},
	messageAvatarImage: {
		borderRadius: 25,
		width: 50,
		aspectRatio: 1,
		marginRight: 10
	},
	messageHeading: {
		fontSize: 18,
		color: 'blue'
	},
	messageSubHeading: {}
});
