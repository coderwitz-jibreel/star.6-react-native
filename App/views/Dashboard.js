import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import Carousel from 'react-native-snap-carousel';
import moment from 'moment';

import {
	TouchableOpacity,
	View,
	StyleSheet,
	Text,
	Image,
	Dimensions,
	AsyncStorage,
	Alert
} from 'react-native';
import { colors, font, API, imageEndPoint } from '../config';
import LogoTitle from '../components/LogoTitle';

class Dashboard extends Component {
	static navigationOptions = ({ navigation }) => ({
		headerTitle: <LogoTitle />,
		headerLeft: (
			<TouchableOpacity
				onPress={() => navigation.openDrawer()}
				title='Info'
				color='#fff'
			>
				<Image
					style={{ height: 25, aspectRatio: 1, marginLeft: 10 }}
					source={require('../assets/menu.png')}
				/>
			</TouchableOpacity>
		),
		drawerLabel: 'Home'
	});

	constructor(props) {
		super(props);

		this.state = {
			matches: [],
			sliderIndex: 0,
			dataFetched: false
		};
	}

	async componentWillMount() {
		try {
			const { response: matches } = await this.getSeraches();
			if (matches) {
				(matches || []).map((c, index) => {
					c.key = c.id;
					c.active = index == 0;
					return c;
				});

				this.setState({ matches, dataFetched: true });
			} else {
				this.setState({ matches: [], dataFetched: true });
			}
			console.log(matches);
		} catch (error) {
			console.log(error);
      this.setState({ matches: [], dataFetched: true });
		}
	}

	async getSeraches() {
		const token = await AsyncStorage.getItem('token');
		try {
			const result = await fetch(API.userSearch, {
				method: 'GET',
				headers: {
					'x-access-token': token
				}
			});

			return await result.json();
		} catch (error) {
			console.log(error);
			return;
		}
	}

	async action(type) {
		const token = await AsyncStorage.getItem('token');
		const user = this.state.matches[this.state.sliderIndex];
		console.log('match request -----');
		console.log({ user_id: user.id, status: type });
		let result = await fetch(API.matchCreate, {
			method: 'POST',
			body: JSON.stringify({ user_id: user.id, status: type }),
			headers: {
				'x-access-token': token,
				'Content-Type': 'application/json'
			}
		});
		result = await result.json();
		console.log(result);
		if (result.status) {
			Alert.alert('SUCCSES', 'liked');
		} else {
			Alert.alert('ERROR', 'something went wrong');
		}
	}

	_onScroll = e => {
		let { matches } = this.state;
		matches = matches.map((entry, index) => ({ ...entry, active: index == e }));
		this.setState({ matches });
	};

	_renderItem = ({ item, index }) => {
		return (
			<View style={styles.slide}>
				<Image
					source={{
						uri: item.photos[0]
							? imageEndPoint + item.photos[0].path
							: `http://placehold.it/500x500/${index}${index}${index}${index}${index}${index}?text=No Image`
					}}
					style={[styles.slideImage]}
				/>
				<SliderText
					show={item.active}
					heading={`${item.firstName} ${item.lastName}`}
					subHeading={`${moment().diff(item.profile.dob, 'years')} years, ${
						item.profile.gender
					}`}
				/>
			</View>
		);
	};

	render() {
		const sliderWidth = Dimensions.get('window').width;
		const itemWidth = sliderWidth - 80;
		if (this.state.matches.length && this.state.dataFetched) {
			return (
				<View style={styles.container}>
					<Carousel
						data={this.state.matches}
						layout={'default'}
						renderItem={this._renderItem}
						sliderWidth={sliderWidth}
						itemWidth={itemWidth}
						itemHeight={100}
						onSnapToItem={this._onScroll}
					/>
					<View style={styles.actionsContainer}>
						<View style={{ flex: 0.25, justifyContent: 'center' }}>
							<TouchableOpacity>
								<Image
									style={styles.actionButton}
									source={require('../assets/h-back.png')}
								/>
							</TouchableOpacity>
						</View>
						<View
							style={{
								flex: 0.5,
								flexDirection: 'row',
								justifyContent: 'center'
							}}
						>
							<TouchableOpacity onPress={() => this.action('accepted')}>
								<Image
									style={styles.actionButton}
									source={require('../assets/h-heart.png')}
								/>
							</TouchableOpacity>
							<TouchableOpacity onPress={() => this.action('rejected')}>
								<Image
									style={styles.actionButton}
									source={require('../assets/h-cross.png')}
								/>
							</TouchableOpacity>
						</View>
						<View style={{ flex: 0.25 }} />
					</View>
				</View>
			);
		} else if (!this.state.dataFetched) {
			return (
				<View
					style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
				>
					<Text>Searching...</Text>
				</View>
			);
		} else {
			return (
				<View
					style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
				>
					<Text>No Matches Found</Text>
				</View>
			);
		}
	}
}

const SliderText = props => {
	if (props.show) {
		return (
			<View style={{ alignItems: 'center' }}>
				<Text style={styles.heading}>{props.heading}</Text>
				<Text style={styles.subHeading}>{props.subHeading}</Text>
				<View style={styles.mutualFriendsContainer}>
					<Image
						style={styles.mutualFriend}
						source={require('../assets/a.jpg')}
					/>
					<Image
						style={styles.mutualFriend}
						source={{ uri: 'http://placehold.it/500x500/cccccc' }}
					/>
					<Image
						style={styles.mutualFriend}
						source={{ uri: 'http://placehold.it/500x500/cccccc' }}
					/>
					<Image
						style={styles.mutualFriend}
						source={{ uri: 'http://placehold.it/500x500/cccccc' }}
					/>
					<View style={styles.moreMutualFriend}>
						<Text style={styles.moreMutualFriendText}>32+</Text>
					</View>
				</View>
				<View>
					<Text>Mutual Friends</Text>
				</View>
			</View>
		);
	} else {
		return <Text />;
	}
};

const Navigation = createStackNavigator(
	{
		Dashboard: { screen: Dashboard }
	},
	{
		initialRouteName: 'Dashboard',
		defaultNavigationOptions: {
			headerStyle: {
				backgroundColor: colors.primary
			},
			headerTintColor: '#fff',
			headerTitleStyle: {
				color: '#ffffff'
			}
		}
	}
);

export default Navigation;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: '#ffffff',
		paddingTop: 30
	},
	slideImage: {
		aspectRatio: 4 / 3,
		width: '100%',
		marginBottom: 10
	},
	heading: {
		fontSize: 20,
		color: colors.primary,
		fontWeight: 'bold'
	},
	subHeading: {
		marginBottom: 10
	},
	mutualFriendsContainer: {
		flexDirection: 'row',
		marginVertical: 5
	},
	mutualFriend: {
		width: 50,
		aspectRatio: 1,
		borderRadius: 25,
		marginHorizontal: 3
	},
	moreMutualFriend: {
		width: 50,
		height: 50,
		borderRadius: 100,
		borderWidth: 1,
		borderColor: colors.grayLight,
		alignItems: 'center',
		justifyContent: 'center'
	},
	moreMutualFriendText: {
		color: colors.primary,
		fontSize: font.size.main,
		fontWeight: 'bold'
	},
	actionsContainer: {
		flexDirection: 'row',
		marginBottom: 60
	},
	actionButton: {
		height: 40,
		aspectRatio: 1,
		marginHorizontal: 5
	}
});
