import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import {
	TouchableOpacity,
	View,
	StyleSheet,
	Text,
	Image,
	Switch,
  FlatList
} from 'react-native';
import { colors, font } from '../config';
import LogoTitle from '../components/LogoTitle';

class Settings extends Component {
	static navigationOptions = ({ navigation }) => ({
		title: 'Settings',
		headerLeft: (
			<TouchableOpacity
				onPress={() => navigation.openDrawer()}
				title='Info'
				color='#fff'
			>
				<Image
					style={{ height: 25, aspectRatio: 1, marginLeft: 10 }}
					source={require('../assets/menu.png')}
				/>
			</TouchableOpacity>
		),
		drawerLabel: 'Settings'
	});
	constructor(props) {
		super(props);
	}

	press() {
		this.props.navigation.navigate();
	}

	render() {
		return (
			<View style={styles.container}>
				<View style={{ width: '95%' }}>
					<View style={styles.heading}>
						<Text style={[styles.font]}>Notifications</Text>
					</View>
					<View>
            <View
              style={{
                flexDirection: 'row',
                borderTopWidth: 1,
                borderColor: '#cecece',
                paddingTop: 7,
                justifyContent: 'space-between',
                width: '100%'
              }}
            >
              <Text style={[styles.font, styles.item]}>Messages</Text>
              <Switch
                value={true}
                trackColor={{ true: colors.primaryLight }}
                thumbColor={colors.primary}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                borderBottomWidth: 1,
                borderColor: '#cecece',
                justifyContent: 'space-between',
                width: '100%'
              }}
            >
              <Text style={[styles.font, styles.item]}>Match</Text>
              <Switch
                value={true}
                trackColor={{ true: colors.primaryLight }}
                thumbColor={colors.primary}
              />
            </View>
					</View>
				</View>
			</View>
		);
	}
}

const Navigation = createStackNavigator(
	{
		Settings: { screen: Settings }
	},
	{
		initialRouteName: 'Settings',
		defaultNavigationOptions: {
			headerStyle: {
				backgroundColor: colors.primary
			},
			headerTintColor: '#fff',
			headerTitleStyle: {
				color: '#ffffff'
			}
		}
	}
);

export default Navigation;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: '#ffffff'
	},
	heading: {
		width: '100%',
		paddingVertical: 10
	},
	font: {
		fontSize: font.size.main
	},
	item: {
		paddingVertical: 10
	}
});
