import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import TagInput from 'react-native-tag-input';
import AutoTags from 'react-native-tag-autocomplete';
import { ImagePicker, Permissions, Location } from 'expo';
import ModalSelector from 'react-native-modal-selector';

import {
	TouchableOpacity,
	TouchableWithoutFeedback,
	View,
	StyleSheet,
	Text,
	Image,
	TextInput,
	KeyboardAvoidingView,
	ScrollView,
	NativeModules,
	AsyncStorage,
	Alert
} from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Spinner from 'react-native-loading-spinner-overlay';

import { colors, API, imageEndPoint } from '../config';

import moment from 'moment';

class Profile extends Component {
	static navigationOptions = ({ navigation }) => ({
		title: 'Profile',
		headerLeft: (
			<TouchableOpacity
				onPress={() => navigation.openDrawer()}
				title='Info'
				color='#fff'
			>
				<Image
					style={{ height: 25, aspectRatio: 1, marginLeft: 10 }}
					source={require('../assets/menu.png')}
				/>
			</TouchableOpacity>
		),
		drawerLabel: 'Profile'
	});
	constructor(props) {
		super(props);
		this.state = {
			music: [],
			musicText: '',
			photos: [
				{ id: '', path: '' },
				{ id: '', path: '' },
				{ id: '', path: '' },
				{ id: '', path: '' },
				{ id: '', path: '' },
				{ id: '', path: '' }
			],
			user: {
				religion_id: 0,
				gender: '',
				seeking: '',
				height: '',
				//
				dob: '',
				location: '',
				kosher: '',
				latitude: '28.7041',
				longitude: '77.1025',
				hometown: '',
				height_feet: 5,
				height_inch: 7,
				//
				occupations: [],
				educations: [],
				questions: [],
				interests: []
			},
			educations: [],
			occupations: [],
			questions: [],
			interests: [],
			religions: [],
			questionsAnswers: [],
			isDateTimePickerVisible: false,
			spinner: false
		};
	}

	async componentWillMount() {
		try {
			const result = await this.getUserDetails();
			if (result.status) {
				this.setUserDetails(result.response);
			}
			console.log(this.state.user);

			await this.getEducations();
			await this.getOccupations();
			await this.getIntrest();
			await this.getQuestions();
			await this.getReligion();
		} catch (error) {
			console.log(error);
		}
	}

	_showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
	_hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
	_handleDatePicked = date => {
    const dob = moment(date).format('YYYY-MM-DD HH:mm:ss');
		this.setState({ user: { ...this.state.user, dob } });
    console.log('dob change '+ dob)
    console.log(this.state.user)
    this.updateProfile();
		this._hideDateTimePicker();
	};

	async getLocation() {
		try {
			let { status } = await Permissions.askAsync(Permissions.LOCATION);
			if (status == 'granted') {
				location = await Location.getCurrentPositionAsync({});
				return location.coords;
			}
			Alert.alert('ERROR', 'Give permission to access location');
			return;
		} catch (error) {
			Alert.alert('ERROR', 'Enable location service');
		}
	}

	async updateProfile() {
		const token = await AsyncStorage.getItem('token');
		const user = this.state.user;
		console.log(user);

		let coords = await this.getLocation();
		if (!coords) {
			coords = { latitude: user.latitude, longitude: user.longitude };
		}
		// "latitude": 28.7004078,
		// "longitude": 77.1296405,

		const questions = {};
		if (user.questions.length >= 1) {
			questions[user.questions[0].id] = {
				answer: this.state.questionsAnswers[0]
			};
		}
		if (user.questions.length >= 2) {
			questions[user.questions[1].id] = {
				answer: this.state.questionsAnswers[1]
			};
		}

		const obj = {
			religion_id: user.religion_id,
			gender: user.gender,
			seeking: user.seeking,
			dob: user.dob,
			kosher: user.kosher,
			location: user.location,
			latitude: coords.latitude,
			longitude: coords.longitude,
			hometown: user.hometown,
			height_feet: user.height_feet,
			height_inch: user.height_inch,
			educations: user.educations.map(e => e.id),
			occupations: user.occupations.map(o => o.id),
			questions,
			interests: user.interests.map(i => i.id)
    };
    console.log('obj ------ request')
    console.log(obj)
		try {
			const result = await fetch(API.profileUpdate, {
				method: 'POST',
				body: JSON.stringify(obj),
				headers: {
					'x-access-token': token,
					'Content-Type': 'application/json'
				}
			});
			const json = await result.json();
			if (json.status) {
				return json;
			}
			console.log(json);
			Alert.alert('ERROR', json.response);
		} catch (error) {
			console.log('update profile error'),
				console.log(error),
				Alert.alert('ERROR', error.response);
		}
		return;
	}

	async changeGender(gender) {
		this.setState({
			user: { ...this.state.user, gender }
		});
		const result = await this.updateProfile();
		console.log(result);
		if (result) {
			console.log(result);
		}
	}
	async changeSeeking(seeking) {
		this.setState({
			user: { ...this.state.user, seeking }
		});
		const result = await this.updateProfile();
		console.log(result);
		if (result) {
			console.log(result);
		}
	}

	async getEducations() {
		const result = await fetch(API.education);
		const { status, response: educations } = await result.json();
		if (status) {
			this.setState({ educations });
			console.log(educations);
		}
		return;
	}

	async getOccupations() {
		const result = await fetch(API.occupation);
		const { status, response: occupations } = await result.json();
		if (status) {
			this.setState({ occupations });
			console.log(occupations);
		}
		return;
	}
	async getIntrest() {
		const result = await fetch(API.interest);
		const { status, response: interests } = await result.json();
		if (status) {
			this.setState({ interests });
			console.log(interests);
		}
		return;
	}
	async getQuestions() {
		const result = await fetch(API.question);
		const { status, response: questions } = await result.json();
		if (status) {
			this.setState({ questions });
			console.log(questions);
		}
		return;
	}
	async getReligion() {
		const result = await fetch(API.religion);
		const { status, response: religions } = await result.json();
		if (status) {
			this.setState({ religions });
			console.log(religions);
		}
		return;
	}

	selectImage = async index => {
		const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
		if (status == 'granted') {
			let result = await ImagePicker.launchImageLibraryAsync({
				allowsEditing: true
				// aspect: [1, 1]
				// base64: true
			});

			if (!result.cancelled) {
				this.setState({ spinner: true });
				console.log(result);
				const uploadResult = await this.uploadImage(result.uri, result.type);
				if (uploadResult.status) {
					let { photos } = this.state;
					photos[index] = {
						path: imageEndPoint + uploadResult.response.path,
						id: uploadResult.response.id
					};
					// this.setState({ photos });

					photos = this.state.photos
						.filter(photo => photo.id)
						.map(photo => photo.id);

					const photoAttachResult = await this.photoAttach(photos);
					this.setPhotos(photoAttachResult.response.photos);
					this.setState({ spinner: false });
				} else {
					console.log('uploadResult error');
					console.log(uploadResult);
				}
			} else {
				this.setState({ spinner: false });
			}
		} else {
			alert('Please give permission');
		}
	};

	async uploadImage(uri, type) {
		const file = {
			uri,
			name: 'photo',
			type: 'image/jpg'
		};

		const body = new FormData();
		body.append('photo', file);

		const token = await AsyncStorage.getItem('token');
		try {
			console.log('photo create hit');
			const response = await fetch(API.photoCreate, {
				method: 'POST',
				body,
				headers: {
					'x-access-token': token
				}
			});

			return await response.json();
		} catch (error) {
			console.log('photo upload error');
			console.log(error);
		}
	}

	async photoAttach(photos) {
		console.log('photo attach hit');
		try {
			const token = await AsyncStorage.getItem('token');
			const result = await fetch(API.userPhotoUpdate, {
				method: 'POST',
				body: JSON.stringify({ photos }),
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'x-access-token': token
				}
			});
			return result.json();
		} catch (error) {
			console.log('photo attach error');
			console.log(error);
		}
	}

	async getUserDetails() {
		const token = await AsyncStorage.getItem('token');
		try {
			const result = await fetch(API.userDetails, {
				method: 'GET',
				headers: {
					'x-access-token': token
				}
			});
			return result.json();
		} catch (error) {
			throw new Error(error);
		}
	}

	async setUserDetails(user) {
		this.setPhotos(user.photos);
		delete user.photos;
		console.log('user.height-----------------');
		console.log(user);
		console.log('user.question-----------------');
		console.log(user.questions.length);
		console.log(user.questions.length >= 1);
		this.setState({
			user: {
				...user.profile,
				height_feet: Math.floor(user.profile.height),
				height_inch: Math.round(
					user.profile.height - Math.floor(user.profile.height) * 12
				),
				occupations: user.occupations,
				educations: user.educations,
				questions: user.questions,
				interests: user.interests
			},
			questionsAnswers: [
				user.questions.length >= 1 ? user.questions[0].pivot.answer : '',
				user.questions.length >= 2 ? user.questions[1].pivot.answer : ''
			]
		});
		return;
	}

	async setPhotos(photos) {
		photos = photos.map(({ id, path }) => ({ id, path: imageEndPoint + path }));
		while (photos.length < 6) {
			photos.push({ id: '', path: '' });
		}
		this.setState({ photos });
		return;
	}

	handleTagAddition(suggestion) {
		const user = this.state.user;
		user.interests = user.interests.concat(suggestion);
		this.setState({ user });
	}

	handleTagDelete(index) {
		const user = this.state.user;
		user.interests.splice(index, 1);
		this.setState({ user });
	}

	render() {
		let index = 0;
		const age = [
			{ key: index++, section: true, label: 'Age' },
			{ key: index++, label: '18' },
			{ key: index++, label: '17' },
			{ key: index++, label: '19' },
			{ key: index++, label: '20' },
			{ key: index++, label: '21' }
		];
		const height = [
			{ key: index++, section: true, label: 'Height' },
			{ key: index++, label: `4'0"`, height_feet: 4, height_inch: 0 },
			{ key: index++, label: `4'1"`, height_feet: 4, height_inch: 1 },
			{ key: index++, label: `4'2"`, height_feet: 4, height_inch: 2 },
			{ key: index++, label: `4'3"`, height_feet: 4, height_inch: 3 },
			{ key: index++, label: `4'4"`, height_feet: 4, height_inch: 4 },
			{ key: index++, label: `4'5"`, height_feet: 4, height_inch: 5 },
			{ key: index++, label: `4'6"`, height_feet: 4, height_inch: 6 },
			{ key: index++, label: `4'7"`, height_feet: 4, height_inch: 7 },
			{ key: index++, label: `4'8"`, height_feet: 4, height_inch: 8 },
			{ key: index++, label: `4'9"`, height_feet: 4, height_inch: 9 },
			{ key: index++, label: `4'10"`, height_feet: 4, height_inch: 10 },
			{ key: index++, label: `4'11"`, height_feet: 4, height_inch: 11 },
			{ key: index++, label: `5'0"`, height_feet: 5, height_inch: 0 },
			{ key: index++, label: `5'1"`, height_feet: 5, height_inch: 1 },
			{ key: index++, label: `5'2"`, height_feet: 5, height_inch: 2 },
			{ key: index++, label: `5'3"`, height_feet: 5, height_inch: 3 },
			{ key: index++, label: `5'4"`, height_feet: 5, height_inch: 4 },
			{ key: index++, label: `5'5"`, height_feet: 5, height_inch: 5 },
			{ key: index++, label: `5'6"`, height_feet: 5, height_inch: 6 },
			{ key: index++, label: `5'7"`, height_feet: 5, height_inch: 7 },
			{ key: index++, label: `5'8"`, height_feet: 5, height_inch: 8 },
			{ key: index++, label: `5'9"`, height_feet: 5, height_inch: 9 },
			{ key: index++, label: `5'10"`, height_feet: 5, height_inch: 10 },
			{ key: index++, label: `5'11"`, height_feet: 5, height_inch: 11 },
			{ key: index++, label: `6'0"`, height_feet: 6, height_inch: 0 },
			{ key: index++, label: `6'1"`, height_feet: 6, height_inch: 1 },
			{ key: index++, label: `6'2"`, height_feet: 6, height_inch: 2 },
			{ key: index++, label: `6'3"`, height_feet: 6, height_inch: 3 },
			{ key: index++, label: `6'4"`, height_feet: 6, height_inch: 4 },
			{ key: index++, label: `6'5"`, height_feet: 6, height_inch: 5 },
			{ key: index++, label: `6'6"`, height_feet: 6, height_inch: 6 },
			{ key: index++, label: `6'7"`, height_feet: 6, height_inch: 7 },
			{ key: index++, label: `6'8"`, height_feet: 6, height_inch: 8 },
			{ key: index++, label: `6'9"`, height_feet: 6, height_inch: 9 },
			{ key: index++, label: `6'10"`, height_feet: 6, height_inch: 10 },
			{ key: index++, label: `6'11"`, height_feet: 6, height_inch: 11 },
			{ key: index++, label: `7'0"`, height_feet: 7, height_inch: 0 },
			{ key: index++, label: `7'1"`, height_feet: 7, height_inch: 1 },
			{ key: index++, label: `7'2"`, height_feet: 7, height_inch: 2 },
			{ key: index++, label: `7'3"`, height_feet: 7, height_inch: 3 },
			{ key: index++, label: `7'4"`, height_feet: 7, height_inch: 4 },
			{ key: index++, label: `7'5"`, height_feet: 7, height_inch: 5 },
			{ key: index++, label: `7'6"`, height_feet: 7, height_inch: 6 },
			{ key: index++, label: `7'7"`, height_feet: 7, height_inch: 7 },
			{ key: index++, label: `7'8"`, height_feet: 7, height_inch: 8 },
			{ key: index++, label: `7'9"`, height_feet: 7, height_inch: 9 },
			{ key: index++, label: `7'10"`, height_feet: 7, height_inch: 10 },
			{ key: index++, label: `7'11"`, height_feet: 7, height_inch: 11 },
			{ key: index++, label: `8'0"`, height_feet: 8, height_inch: 0 },
			{ key: index++, label: `8'1"`, height_feet: 8, height_inch: 1 },
			{ key: index++, label: `8'2"`, height_feet: 8, height_inch: 2 },
			{ key: index++, label: `8'3"`, height_feet: 8, height_inch: 3 },
			{ key: index++, label: `8'4"`, height_feet: 8, height_inch: 4 },
			{ key: index++, label: `8'5"`, height_feet: 8, height_inch: 5 },
			{ key: index++, label: `8'6"`, height_feet: 8, height_inch: 6 },
			{ key: index++, label: `8'7"`, height_feet: 8, height_inch: 7 },
			{ key: index++, label: `8'8"`, height_feet: 8, height_inch: 8 },
			{ key: index++, label: `8'9"`, height_feet: 8, height_inch: 9 },
			{ key: index++, label: `8'10"`, height_feet: 8, height_inch: 10 },
			{ key: index++, label: `8'11"`, height_feet: 8, height_inch: 11 }
		];
		const kosher = [
			{ key: index++, label: `kosher one` },
			{ key: index++, label: `kosher two` },
			{ key: index++, label: `kosher three` }
		];
		return (
			<View style={{ flex: 1 }}>
				<Spinner
					visible={this.state.spinner}
					textContent={'Uploading...'}
					textStyle={styles.spinnerTextStyle}
				/>
				<KeyboardAvoidingView behavior='padding' style={{ flex: 1 }}>
					<ScrollView style={{ flex: 1 }}>
						<View style={styles.container}>
							<View style={styles.profileImagesContainer}>
								{this.state.photos.map((photo, index) => (
									<TouchableOpacity
										key={index}
										onPress={() => this.selectImage(index)}
									>
										<Image
											source={
												photo.path
													? { uri: photo.path }
													: require('../assets/add-icon.png')
											}
											style={styles.profileImage}
										/>
									</TouchableOpacity>
								))}
							</View>
							<View style={[styles.container, { flexDirection: 'row' }]}>
								<View style={{ flex: 0.8 }}>
									<Text>Gender</Text>
								</View>
								<View style={{ flex: 1, paddingHorizontal: 5 }}>
									<TouchableWithoutFeedback
										onPress={() => this.changeGender('male')}
									>
										<Text
											style={[
												styles[
													this.state.user.gender == 'male'
														? 'radioActive'
														: 'radio'
												]
											]}
										>
											Male
										</Text>
									</TouchableWithoutFeedback>
								</View>
								<View style={{ flex: 1, paddingHorizontal: 5 }}>
									<TouchableWithoutFeedback
										onPress={() => this.changeGender('female')}
									>
										<Text
											style={[
												styles[
													this.state.user.gender == 'female'
														? 'radioActive'
														: 'radio'
												]
											]}
										>
											Female
										</Text>
									</TouchableWithoutFeedback>
								</View>
							</View>
							<View style={[styles.container, { flexDirection: 'row' }]}>
								<View style={{ flex: 0.8 }}>
									<Text>Seeking</Text>
								</View>
								<View style={{ flex: 1, paddingHorizontal: 5 }}>
									<TouchableWithoutFeedback
										onPress={() => this.changeSeeking('men')}
									>
										<Text
											style={[
												styles[
													this.state.user.seeking == 'men'
														? 'radioActive'
														: 'radio'
												]
											]}
										>
											Men
										</Text>
									</TouchableWithoutFeedback>
								</View>
								<View style={{ flex: 1, paddingHorizontal: 5 }}>
									<TouchableWithoutFeedback
										onPress={() => this.changeSeeking('woman')}
									>
										<Text
											style={[
												styles[
													this.state.user.seeking == 'woman'
														? 'radioActive'
														: 'radio'
												]
											]}
										>
											Woman
										</Text>
									</TouchableWithoutFeedback>
								</View>
							</View>
							<View style={styles.inputContainer}>
								<TextInput
									style={styles.input}
									value={this.state.user.location}
									onChange={e => {
										console.log(e);
										this.setState({
											user: { ...this.state.user, location: e.nativeEvent.text }
										});
									}}
									onBlur={() => {
										this.updateProfile();
									}}
									placeholder='Location'
								/>
							</View>
							<View style={styles.inputContainer}>
								<TextInput
									style={styles.input}
									value={this.state.user.hometown}
									onChange={e => {
										this.setState({
											user: { ...this.state.user, hometown: e.nativeEvent.text }
										});
									}}
									onBlur={() => {
										this.updateProfile();
									}}
									placeholder='Hometown'
								/>
							</View>
							<View style={styles.inputContainer}>
								<TouchableOpacity onPress={this._showDateTimePicker}>
									<Text style={{ paddingHorizontal: 10, paddingVertical: 15 }}>
										{this.state.user.dob
											? moment(this.state.user.dob).format('MMMM DD, YYYY')
											: 'DOB'}
									</Text>
								</TouchableOpacity>
								<DateTimePicker
									isVisible={this.state.isDateTimePickerVisible}
									onConfirm={this._handleDatePicked}
									onCancel={this._hideDateTimePicker}
								/>
							</View>
							{/* <View style={styles.inputContainer}>
								<ModalSelector
									data={age}
									initValue='Age'
									onChange={option => {
										alert(`${option.label}`);
									}}
								/>
							</View> */}
							<View style={styles.inputContainer}>
								<ModalSelector
									data={height}
									initValue={
										this.state.user.height
											? `${Math.floor(this.state.user.height)}'${Math.round(
													(this.state.user.height -
														Math.floor(this.state.user.height)) *
														12
											  )}"`
											: 'Height'
									}
									onChange={({ height_feet, height_inch }) => {
										this.setState({
											user: { ...this.state.user, height_feet, height_inch }
										});
										this.updateProfile();
									}}
								/>
							</View>
							<View style={styles.inputContainer}>
								<ModalSelector
									data={this.state.occupations}
									initValue={
										this.state.user.occupations.length
											? this.state.user.occupations[0].name
											: 'Occupation'
									}
									keyExtractor={item => item.id}
									labelExtractor={item => item.name}
									onChange={occupation => {
										this.setState({
											user: { ...this.state.user, occupations: [occupation] }
										});
										this.updateProfile();
									}}
								/>
							</View>
							<View style={styles.inputContainer}>
								<ModalSelector
									data={this.state.educations}
									initValue={
										this.state.user.educations.length
											? this.state.user.educations[0].name
											: 'Education'
									}
									keyExtractor={item => item.id}
									labelExtractor={item => item.name}
									onChange={education => {
										this.setState({
											user: { ...this.state.user, educations: [education] }
										});
										this.updateProfile();
									}}
								/>
							</View>
							<View style={styles.inputContainer}>
								<TextInput style={styles.input} placeholder='About me' />
							</View>

							<View style={styles.inputContainer}>
								<ModalSelector
									data={this.state.questions.filter(
										q =>
											this.state.user.questions.findIndex(
												sq => sq.id == q.id
											) == -1
									)}
									initValue={
										this.state.user.questions.length >= 1
											? this.state.user.questions[0].name
											: 'Your new year resolution?'
									}
									keyExtractor={item => item.id}
									labelExtractor={item => item.name}
									onChange={question => {
										const { questions } = this.state.user;
										if (questions.length >= 1) {
											questions.push(question);
										} else {
											questions[0] = question;
										}
										this.setState({
											user: { ...this.state.user, questions }
										});
										this.updateProfile();
									}}
								/>
							</View>

							<View style={styles.inputContainer}>
								<TextInput
									style={styles.input}
									placeholder='Answer'
									value={this.state.questionsAnswers[0]}
									onChange={e => {
										this.setState({
											questionsAnswers: [
												e.nativeEvent.text,
												this.state.questionsAnswers[1]
											]
										});
									}}
									onBlur={() => {
										this.updateProfile();
									}}
								/>
							</View>

							<View style={styles.inputContainer}>
								<ModalSelector
									data={this.state.questions.filter(
										q =>
											this.state.user.questions.findIndex(
												sq => sq.id == q.id
											) == -1
									)}
									initValue={
										this.state.user.questions.length >= 2
											? this.state.user.questions[1].name
											: 'Your new year resolution?'
									}
									keyExtractor={item => item.id}
									labelExtractor={item => item.name}
									onChange={question => {
										const { questions } = this.state.user;
										if (questions.length >= 2) {
											questions.push(question);
										} else {
											questions[1] = question;
										}
										this.setState({
											user: { ...this.state.user, questions }
										});
										this.updateProfile();
									}}
								/>
							</View>

							<View style={styles.inputContainer}>
								<TextInput
									style={styles.input}
									placeholder='Answer'
									value={this.state.questionsAnswers[1]}
									onChange={e => {
										this.setState({
											questionsAnswers: [
												this.state.questionsAnswers[0],
												e.nativeEvent.text
											]
										});
									}}
									onBlur={() => {
										this.updateProfile();
									}}
								/>
							</View>

							<View style={styles.inputContainer}>
								<ModalSelector
									data={this.state.religions}
									initValue={
										this.state.user.religion
											? this.state.user.religion.name
											: 'Religion'
									}
									keyExtractor={item => item.id}
									labelExtractor={item => item.name}
									onChange={religion => {
										this.setState({
											user: { ...this.state.user, religion_id: religion.id }
										});
										this.updateProfile();
									}}
								/>
							</View>
							<View style={styles.inputContainer}>
								<ModalSelector
									data={kosher}
									initValue='Kosher'
									onChange={option => {
										alert(`${option.label}`);
									}}
								/>
							</View>
						</View>

						<View style={styles.heading}>
							<Text>Interest</Text>
						</View>
						<View style={styles.container}>
							<Text>Music</Text>
							<View style={styles.inputContainer}>
								<AutoTags
									suggestions={this.state.interests.filter(
										inrest => inrest.type == 'music'
									)}
									autoFocus={false}
									tagsSelected={this.state.user.interests.filter(
										inrest => inrest.type == 'music'
									)}
									handleAddition={this.handleTagAddition.bind(this)}
									handleDelete={this.handleTagDelete.bind(this)}
									placeholder='Type here...'
								/>
							</View>

							<Text>Hobbies</Text>
							<View style={styles.inputContainer}>
								<AutoTags
									suggestions={this.state.interests.filter(
										inrest => inrest.type == 'hobbies'
									)}
									autoFocus={false}
									tagsSelected={this.state.user.interests.filter(
										inrest => inrest.type == 'hobbies'
									)}
									handleAddition={this.handleTagAddition.bind(this)}
									handleDelete={this.handleTagDelete.bind(this)}
									placeholder='Type here...'
								/>
							</View>

							<Text>Movie / TV Genere</Text>
							<View style={styles.inputContainer}>
								<AutoTags
									suggestions={this.state.interests.filter(
										inrest => inrest.type == 'movie / tv genre'
									)}
									autoFocus={false}
									tagsSelected={this.state.user.interests.filter(
										inrest => inrest.type == 'movie / tv genre'
									)}
									handleAddition={this.handleTagAddition.bind(this)}
									handleDelete={this.handleTagDelete.bind(this)}
									placeholder='Type here...'
								/>
							</View>
						</View>
					</ScrollView>
				</KeyboardAvoidingView>
			</View>
		);
	}
}

const Navigation = createStackNavigator(
	{
		Profile: { screen: Profile }
	},
	{
		initialRouteName: 'Profile',
		defaultNavigationOptions: {
			headerStyle: {
				backgroundColor: colors.primary
			},
			headerTintColor: '#fff',
			headerTitleStyle: {
				color: '#ffffff'
			}
		}
	}
);

export default Navigation;

// const tag = (tags)=>{
//   return (
//       <View style={styles.tags}>
//         {tags.map((t, i) => {
//           return (
//             <TouchableHighlight
//               key={i}
//               style={[{marginTop: 5}, styles.tag]}
//               onPress={() => this.props.handleDelete(i)}
//             >
//               <Text>{t.name}</Text>
//             </TouchableHighlight>
//           );
//         })}
//       </View>
//     );
// }

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 15
	},
	radio: {
		paddingVertical: 5,
		paddingHorizontal: 10,
		borderWidth: 1,
		borderColor: colors.gray,
		color: colors.gray,
		textAlign: 'center',
		borderRadius: 15
	},
	radioActive: {
		paddingVertical: 5,
		paddingHorizontal: 10,
		color: '#fff',
		textAlign: 'center',
		backgroundColor: colors.primary,
		borderRadius: 15,
		overflow: 'hidden'
	},
	profileImagesContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	profileImage: {
		aspectRatio: 1,
		height: 40,
		marginVertical: 5,
		borderRadius: 2
	},
	inputContainer: {
		backgroundColor: colors.grayLight,
		borderColor: 'rgba(0,0,0,0.5)',
		borderWidth: 1,
		margin: 3,
		borderRadius: 3,
		marginBottom: 10
	},
	input: {
		paddingVertical: 10,
		paddingHorizontal: 10
	},
	heading: {
		backgroundColor: colors.gray,
		paddingVertical: 10,
		paddingHorizontal: 15
	},
	spinnerTextStyle: {
		color: '#fff'
	}
});
