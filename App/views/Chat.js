import React, { Component } from 'react';
import {
	KeyboardAvoidingView,
	View,
	StyleSheet,
	FlatList,
	TouchableOpacity,
	Text,
	Image,
	AsyncStorage,
  NetInfo,
  Vibration
} from 'react-native';
import Message from '../components/Message';
import SendMessage from '../components/SendMessage';
import { font, colors, imageEndPoint, API } from '../config';
import moment from 'moment';

let interval;

export default class Chat extends Component {
	static navigationOptions = ({ navigation }) => ({
		title: navigation.getParam('title')
	});

	constructor(props) {
		super(props);

		this.state = {
			messages: []
		};
	}

	async componentWillMount() {
		this.getMesasges();

		interval = setInterval(() => {
			NetInfo.getConnectionInfo().then(connectionInfo => {
				if (
					connectionInfo.type == 'wifi' ||
					connectionInfo.type == 'cellular'
				) {
					this.getMesasges();
				}
			});
		}, 3000);
	}

	async componentWillUnmount() {
		this.props.navigation.state.params.onBack();
		clearInterval(interval);
  }
  
  componentWillUpdate(nextProps, nextState) {
    if(this.state.messages.length != 0){
      if(nextState.messages.length != this.state.messages.length){
        Vibration.vibrate(300)
      }
    }
  }

	async acceptMatch() {
		const user = this.props.navigation.getParam('user');
		const token = await AsyncStorage.getItem('token');

		const obj = {
			sender_id: user.id,
			status: 'accepted'
		};

		const result = await fetch(API.acceptMatch, {
			method: 'POST',
			body: JSON.stringify(obj),
			headers: {
				'x-access-token': token,
				'Content-Type': 'application/json'
			}
		});
		const json = await result.json();
		console.log('accept result');
		console.log(json);
		if (json.status) {
			this.props.navigation.setParams({ isMatch: false });
			return json;
		}
		Alert.alert('ERROR', json.response);
		return;
	}

	async getMesasges() {
		const user = this.props.navigation.getParam('user');
		const token = await AsyncStorage.getItem('token');

		const result = await fetch(API.messageChat(user.id), {
			method: 'GET',
			headers: {
				'x-access-token': token
			}
		});

		const { status, response: messages } = await result.json();
		messages.sort((A, B) => {
			const a = A.created_at;
			const b = B.created_at;
			if (a > b) {
				return -1;
			}
			if (a < b) {
				return 1;
			}
			return 0;
		});
		if (status) this.setState({ messages });
	}

	_onMessageSend(message) {
		console.log('message on send');
		const { messages } = this.state;
		messages.unshift(message);
		console.log(messages);
		this.setState({ messages });
	}

	render() {
		user = this.props.navigation.getParam('user');
		if (this.props.navigation.getParam('isMatch')) {
			return (
				<View
					style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
				>
					<Image
						style={styles.profileImage}
						source={
							!user.photos.length
								? require('../assets/profile.png')
								: { uri: imageEndPoint + user.photos[0].path }
						}
					/>
					<Text style={{ textAlign: 'center' }}>
						{user.firstName} {user.lastName} liked you
					</Text>
					<Text style={{ textAlign: 'center' }}>Like back to start chat</Text>
					<TouchableOpacity onPress={this.acceptMatch.bind(this)}>
						<Image
							style={styles.actionButton}
							source={require('../assets/h-heart.png')}
						/>
					</TouchableOpacity>
				</View>
			);
		} else {
			return (
				<KeyboardAvoidingView behavior='padding' style={{ flex: 1 }}>
					<View style={styles.container}>
						<FlatList
							inverted
							style={styles.chatContainer}
							data={this.state.messages}
							extraData={this.state}
							keyExtractor={item => item.id.toString()}
							renderItem={({ item, index }) => (
								<Message
									message={`${item.message}`}
									my={item.sender_id != user.id}
									time={moment(item.created_at).format('DD MM, YY | hh:mm A')}
									image={
										!user.photos.length
											? require('../assets/profile.png')
											: { uri: imageEndPoint + user.photos[0].path }
									}
								/>
							)}
						/>
						<SendMessage
							userId={user.id}
							onSend={message => this._onMessageSend(message)}
						/>
					</View>
				</KeyboardAvoidingView>
			);
		}
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	chatContainer: {
		flex: 1
	},
	acceptButton: {
		fontSize: font.size.main,
		paddingHorizontal: 20,
		paddingVertical: 10,
		marginVertical: 5,
		borderColor: colors.secondry,
		color: colors.secondry,
		alignSelf: 'center',
		borderWidth: 1
	},
	actionButton: {
		height: 60,
		aspectRatio: 1,
		marginTop: 10
	},
	profileImage: {
		height: 100,
		aspectRatio: 1,
		borderRadius: 50,
		marginBottom: 10
	}
});
