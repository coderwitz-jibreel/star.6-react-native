import React from 'react';
import { View, Image } from 'react-native';

SplashImage = require('../assets/splash.png');

export default (SplashScreen = () => (
	<View style={{ flex: 1 }}>
		<Image style={{ flex: 1 }} source={SplashImage} />
	</View>
));
