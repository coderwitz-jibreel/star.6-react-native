import React, { Component } from 'react';
import { AsyncStorage, WebView } from 'react-native';
import { createAppContainer } from 'react-navigation';
import SideMenu from './App/components/SideMenu';
import Home from './App/views/Home';
import SplashScreen from './App/views/SplashScreen';
import { API } from './App/config';

const AppContainer = createAppContainer(SideMenu);

export default class App extends Component {
	constructor(props) {
		super(props);
		this.state = { isLoggedIn: false, isChecking: true, loging: false };
	}

	async componentWillMount() {
		try {
      // await AsyncStorage.removeItem('token');
      const result = await AsyncStorage.getItem('token');
      console.log(result)
      // !!result
			this.setState({ isLoggedIn: !!result, isChecking: false });
		} catch (error) {
			Alert.alert('something went wrong!');
		}
	}

	login() {
		this.setState({ loging: true });
	}

	webViewMonitor(event) {
		const { url } = event;
		if (url.includes('api.star.6.coderwitz.com/user/login/facebook')) {
			const [, paramsString] = url.split('?');
			const [, token] = paramsString.split('=');
			if (token) {
        console.log(token)
				AsyncStorage.setItem('token', token).then(() => {
					this.setState({ isLoggedIn: true });
				});
			} else {
				Alert.alert('something went wrong!');
			}
			this.setState({ loging: false });
		}
	}

	render() {
		if (this.state.loging) {
			return (
				<WebView
					source={{ uri: API.login }}
					onNavigationStateChange={this.webViewMonitor.bind(this)}
				/>
			);
		} else if (!this.state.isChecking && this.state.isLoggedIn) {
			return <AppContainer />;
		} else if (!this.state.isChecking && !this.state.isLoggedIn) {
			return <Home onLogin={this.login.bind(this)} />;
		}
		return <SplashScreen />;
	}
}
